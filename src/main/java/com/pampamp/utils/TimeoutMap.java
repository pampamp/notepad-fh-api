package com.pampamp.utils;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.lang.ref.WeakReference;
import java.util.*;

public class TimeoutMap<K, T> {

    protected static final Logger log = LogManager.getLogger(TimeoutMap.class);

    static private Timer timer = new Timer(true);

    private static final int TIME_BETWEEN_CLEANUPS = (1000 * 60) * 1; // 1 mins
    private static List<WeakReference<TimeoutMap<?, ?>>> allMaps = new ArrayList<WeakReference<TimeoutMap<?, ?>>>();

    static {
        // This runs as soon as the first instance is created
        timer.schedule(new CleanupTask(), TIME_BETWEEN_CLEANUPS, TIME_BETWEEN_CLEANUPS);
    }

    private Map<K, MapElement<T>> valuesMap = null;
    private String description = null;
    private long defaultTimeout = 300000;// 5 minutes

    public TimeoutMap(int initialCapacity, String description, long defaultTimeout) {
        this.description = description;
        if (initialCapacity > 0) {
            valuesMap = Collections.synchronizedMap(new HashMap<K, MapElement<T>>(initialCapacity));
        } else {
            valuesMap = Collections.synchronizedMap(new HashMap<K, MapElement<T>>());
        }
        if (defaultTimeout > 0) {
            this.defaultTimeout = defaultTimeout;
        }
        allMaps.add(new WeakReference<TimeoutMap<?, ?>>(this));
    }

    public TimeoutMap(String description, long defaultTimeout) {
        this(-1, description, defaultTimeout);
    }

    public TimeoutMap(String description) {
        this(-1, description, -1);
    }

    public void put(K key, T value) {
        put(key, value, defaultTimeout);
    }

    synchronized public void put(K key, T value, long timeout) {
        if (key != null && value != null) {
            valuesMap.put(key, new MapElement<T>(value, timeout));
        }
    }

    synchronized public T get(K key) {
        MapElement<T> o = valuesMap.get(key);
        if (o == null)
            return null;
        if (o.expired()) {
            valuesMap.remove(key);
            return null;
        }
        return o.getValue();
    }

    synchronized public void clear() {
        int init = valuesMap.size();
        List<K> keys = new ArrayList<>(valuesMap.keySet());
        for (K key : keys) {
            this.remove(key);
        }
        log.debug(String.format("TimeoutMap.clear [%s] [%d]", description, init));
    }

    public static void clearAllMaps() {
        synchronized (TimeoutMap.class) {
            for (WeakReference<TimeoutMap<?, ?>> reference : allMaps) {
                TimeoutMap<?, ?> ht = reference.get();
                if (ht != null) {
                    ht.clear();
                }
            }
        }
    }

    synchronized public void remove(K key) {
        valuesMap.remove(key);
    }

    synchronized public void removeKeys(String regularEx) {
        K[] keys = (K[])valuesMap.keySet().toArray(); //need to convert to array to avoid java.util.ConcurrentModificationException
        for (K key : keys) {
            Object value = this.get(key); //call to 'get' will check expireTime and remove if necessary
            if (value == null) return;

            if (! (key instanceof String))
                continue;
            String strKey=(String)key;
            if (strKey.matches(regularEx))
                valuesMap.remove(key);
        }
    }
    
    synchronized private void cleanupMap() {
        int init = valuesMap.size();
        List<K> keys = new ArrayList<>(valuesMap.keySet());
        for (K key : keys) {
            // call to 'get' will check expireTime and remove if necessary
            this.get(key);
        }
        int fina = valuesMap.size();
        log.debug(String.format("TimeoutMap.cleanupMap [%s] [%d][%d]", description, init, fina));
    }

    public static void cleanupAllMaps() {
        synchronized (TimeoutMap.class) {
            for (int i = allMaps.size() - 1; i >= 0; i--) {
                WeakReference<TimeoutMap<?, ?>> reference = allMaps.get(i);
                if(reference == null){
                    log.error("weakReference cannot be null in cleanupAllMaps");
                    allMaps.remove(i);
                    continue;
                }
                TimeoutMap<?, ?> ht = reference.get();
                if (ht != null)
                    ht.cleanupMap();
                else {
                    allMaps.remove(i);
                }
            }
        }
    }

    private static class CleanupTask extends TimerTask {
        @Override
        public void run() {
            try {
                cleanupAllMaps();
            } catch (Exception e) {
                log.error("CleanupTask: ErrorWrapper inside cleanupAllMaps()", e);
            }
        }
    }

    private static class MapElement<T> {
        private T value;
        private long expireTime;

        boolean expired() {
            return expireTime < System.currentTimeMillis();
        }

        T getValue() {
            return value;
        }

        MapElement(T value, long timeout) {
            this.value = value;
            this.expireTime = System.currentTimeMillis() + timeout;
        }
    }
}
