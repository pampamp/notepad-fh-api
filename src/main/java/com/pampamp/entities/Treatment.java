package com.pampamp.entities;


public enum Treatment {
    PROPHYLAXIS,
    HEMORRHAGE,
    CONTINUING_TREATMENT;
}
