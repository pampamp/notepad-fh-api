package com.pampamp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

// if you do not use component scan annotation you should put this file in root directory com.pampamp
@SpringBootApplication
@EnableJpaAuditing
public class NotepadStart {

    public static void main(String [] args) {
        SpringApplication.run(NotepadStart.class, args);
    }

}
