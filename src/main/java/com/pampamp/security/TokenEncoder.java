package com.pampamp.security;

import com.pampamp.exception.TokenException;
import com.pampamp.security.entities.Token;


public interface TokenEncoder {
    String encode(Token token) throws TokenException;
    Token decode(String encoded) throws TokenException;
}
