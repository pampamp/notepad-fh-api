package com.pampamp.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.pampamp.enums.ErrorsEnum;
import com.pampamp.exception.TokenException;
import com.pampamp.security.entities.Token;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class TokenEncoderImpl implements TokenEncoder {

  protected static final Logger logger = LogManager.getLogger(TokenEncoderImpl.class);

  @Value("${token.key}")
  private String tokenKey;


  public String encode(Token token) throws TokenException {
    try {
      Algorithm algorithm = Algorithm.HMAC256(tokenKey);
      return JWT.create()
              .withClaim("userId", token.getUserId())
              .withExpiresAt(new Date(System.currentTimeMillis() + token.getExpireTime()))
              .withIssuedAt(new Date(System.currentTimeMillis()))
              .sign(algorithm);
    } catch(Exception e) {
      logger.error(ErrorsEnum.INVALID_TOKEN, e);
      throw new TokenException(e.getMessage());
    }
  }

  public Token decode(String token) throws TokenException {
    try {
      Algorithm algorithm = Algorithm.HMAC256(tokenKey);
      JWTVerifier verifier = JWT.require(algorithm).build();
      DecodedJWT jwt = verifier.verify(token);
      long userId = jwt.getClaim("userId").asLong();
      Long expireTime = jwt.getExpiresAt().getTime();
      return new Token(userId, expireTime);
    }
    catch (Exception e) {
      throw new TokenException(ErrorsEnum.INVALID_TOKEN.getTitle());
    }
  }

}
