package com.pampamp.security;

import org.springframework.stereotype.Service;

import java.security.SecureRandom;

@Service
public class RandomStringImpl implements RandomString {

    private static final String DEFAULT_CODEC = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    public String generateRandom16Bytes() {
        return generateRandomNbytes(16);
    }

    public String generateRandomNbytes(int lenght) {

        StringBuilder randomResult = new StringBuilder();

        SecureRandom ranGen = new SecureRandom();
        for (int i=0; i<lenght; i++) {
            randomResult.append(DEFAULT_CODEC.charAt(ranGen.nextInt(DEFAULT_CODEC.length()-1)));
        }
        return randomResult.toString();
    }

}
