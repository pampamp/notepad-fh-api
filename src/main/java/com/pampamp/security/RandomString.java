package com.pampamp.security;

public interface RandomString {
    String generateRandom16Bytes();
    String generateRandomNbytes(int lenght);
}
