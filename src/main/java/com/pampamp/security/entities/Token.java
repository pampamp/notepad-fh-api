package com.pampamp.security.entities;

public class Token {
    private long userId;
    private long expireTime;

    public Token(long userId, long time) {
        this.userId = userId;
        this.expireTime = time;
    }

    public boolean isExpired() {
        return System.currentTimeMillis() > this.expireTime;
    }

    public long getUserId() {
        return userId;
    }

    public long getExpireTime() {
        return expireTime;
    }
}
