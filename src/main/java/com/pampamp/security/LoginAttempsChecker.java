package com.pampamp.security;

import com.pampamp.utils.TimeoutMap;
import org.apache.commons.lang.Validate;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicInteger;


public class LoginAttempsChecker {

    private static final long INVALID_ACCESS_KEY_TIME_TO_LIVE = 1000L * 60L * 60L; // 1 hour
    
    private static final TimeoutMap<String, InvalidAccess> loginAttemps = new TimeoutMap<String, InvalidAccess>("LoginAttempsChecker.loginAttemps");

    private final int MAX_ATTEMPS = 10;
    private static final String PREFIX = "EMAIL_##";

    public boolean isExcedded(String email) {

        String key = getKey(email);

        InvalidAccess invalidAccess = loginAttemps.get(key);
        if (invalidAccess == null) {
            invalidAccess = new InvalidAccess(email, MAX_ATTEMPS);
            loginAttemps.put(key, invalidAccess, INVALID_ACCESS_KEY_TIME_TO_LIVE);
        }
        invalidAccess.incrementAmount();
        return invalidAccess.isExcedded();
    }

    private String getKey(String email) {
        return PREFIX + email;
    }

    public void clear(String email) {
        String key = getKey(email);
        loginAttemps.remove(key);
    }

    static class InvalidAccess implements Serializable {

        private static final long serialVersionUID = -1507746476655725985L;

        private AtomicInteger amount;
        private int maxAttemps;

        public InvalidAccess(String email, int maxAttemps) {
            Validate.notEmpty(email, "email can not be empty");
            this.maxAttemps = maxAttemps;
            amount = new AtomicInteger();
        }

        public void incrementAmount() {
            this.amount.incrementAndGet();
        }

        public boolean isExcedded() {
            return amount.intValue() >= maxAttemps;
        }
    }
}
