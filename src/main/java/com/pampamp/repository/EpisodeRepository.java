package com.pampamp.repository;


import com.pampamp.entities.Episode;
import com.pampamp.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;;

import java.util.List;

public interface EpisodeRepository extends JpaRepository<Episode, Long> {
    List<Episode> findByUserOrderByIdDesc(User user);

    @Query("SELECT e FROM Episode e WHERE user = :user order by id desc")
    List<Episode> findFirstOrderByIdDescByUser(@Param("user") User user);
}
