package com.pampamp.enums;

public enum ErrorsEnum {
    INVALID_TOKEN("Token invalido"),
    TOKEN_EXPIRED("Token expirado"),
    USER_NOT_FOUND("Usuario no encontrado"),
    MAX_LOGIN_ATTENDS_EXEDED("Demasiados de itentos de login fallidos"),
    EMAIL_OR_PASSWORD_BLANK("Email o contraseña vacia"),
    INVALID_EMAIL_PATTERN("El email no tiene un formato valido"),
    INVALID_PASSWORD_PATTERN("El password debe contener al menos una letra mayuscula una minuscula un número y un largo de entre 6 y 20"),
    EMAIL_NOT_EXIST("La cuenta no existe en la base de datos"),
    INVALID_PASSWORD("Usuario o contraña invalida"),
    PASSWORD_NOT_MATCH("Las contraseñas no coinciden"),
    EMAIL_ALREADY_EXIST("El email ya existe en la base de datos"),

    GENERAL_ERROR("Error general");

    private String title;
    ErrorsEnum(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
