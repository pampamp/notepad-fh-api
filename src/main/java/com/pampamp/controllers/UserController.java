package com.pampamp.controllers;

import com.pampamp.dtos.ErrorWrapper;
import com.pampamp.entities.User;
import com.pampamp.exception.NotepadException;
import com.pampamp.exception.TokenException;
import com.pampamp.services.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {

    protected static final Logger LOGGER = LogManager.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getByToken(@RequestHeader("token") String token){
        try {
            return new ResponseEntity(userService.getUserByToken(token), HttpStatus.ACCEPTED);
        } catch (TokenException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.UNAUTHORIZED);
        } catch (NotepadException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/user", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestHeader("token") String token, @RequestBody User user){
        try {
            return new ResponseEntity(userService.save(user, token), HttpStatus.ACCEPTED);
        } catch (TokenException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.UNAUTHORIZED);
        } catch (NotepadException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
