package com.pampamp.controllers;

import com.pampamp.dtos.ErrorWrapper;
import com.pampamp.exception.LoginException;
import com.pampamp.exception.NotepadException;
import com.pampamp.services.AccountService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/api")
public class AccountController {

    protected static final Logger LOGGER = LogManager.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/authenticateaccount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity authenticateAccount(HttpServletRequest request,
                                                 @RequestParam(value = "email") String email,
                                                 @RequestParam(value = "password") String password) {
        try {
            return new ResponseEntity(accountService.authenticateAccount(email, password), HttpStatus.ACCEPTED);
        } catch (LoginException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.UNAUTHORIZED);
        } catch (NotepadException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "/createaccount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
     public ResponseEntity createAccount(HttpServletRequest request,
                                                @RequestParam(value = "email") String email,
                                                @RequestParam(value = "password") String password,
                                                @RequestParam(value = "repassword") String repassword
    ) {
        try {
            return new ResponseEntity(accountService.createAccount(email, password, repassword), HttpStatus.CREATED);
        } catch (LoginException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.UNAUTHORIZED);
        } catch (NotepadException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
