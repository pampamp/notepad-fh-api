package com.pampamp.controllers;

import com.pampamp.entities.Episode;
import com.pampamp.dtos.ErrorWrapper;
import com.pampamp.exception.NotepadException;
import com.pampamp.exception.TokenException;
import com.pampamp.repository.EpisodeRepository;
import com.pampamp.services.UserService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping("/api")
public class EpisodeController {

    protected static final Logger LOGGER = LogManager.getLogger(EpisodeController.class);

    @Autowired
    EpisodeRepository episodeRepository;

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET, value = "/episode", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAll(@RequestHeader("token") String token){
        try {
            return new ResponseEntity(episodeRepository.findByUserOrderByIdDesc(userService.getUserByToken(token)), HttpStatus.OK);
        } catch (TokenException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.UNAUTHORIZED);
        } catch (NotepadException e) {
            LOGGER.warn("error in " + this.getClass(), e);
            return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/episode", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity save(@RequestHeader("token") String token, @RequestBody Episode episode){
        episode.setDate(new Date());
         try {
             episode.setUser(userService.getUserByToken(token));
            return new ResponseEntity(episodeRepository.save(episode), HttpStatus.CREATED);
         } catch (TokenException e) {
             LOGGER.warn("error in " + this.getClass(), e);
             return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.UNAUTHORIZED);
         } catch (NotepadException e) {
             LOGGER.warn("error in " + this.getClass(), e);
             return new ResponseEntity(new ErrorWrapper(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
         }
    }
}
