package com.pampamp.exception;


public class TokenException extends NotepadException {

  public TokenException(String s) {
    super(s);
  }
}
