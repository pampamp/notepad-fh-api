package com.pampamp.exception;

public class LoginException extends NotepadException {

    private String internalMessage = "";

    public LoginException(String s, Exception e) {
        super(s, e);
    }

    public LoginException(String s) {
        super(s);
    }

    public LoginException(String s, String internalMessage) {
        super(s);
        this.internalMessage = internalMessage;
    }

    public LoginException(Exception e) {
        super(e);
    }

    public String getInternalMessage() {
        return this.internalMessage;
    }
}
