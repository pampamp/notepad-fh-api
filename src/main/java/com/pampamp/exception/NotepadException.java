package com.pampamp.exception;

import com.pampamp.enums.ErrorsEnum;

public class NotepadException extends Exception {

    private ErrorsEnum error = null;

    public NotepadException(ErrorsEnum error, String s, Exception e) {
        super(s, e);
        this.error = error;
    }

    public NotepadException(String s, Exception e) {
        super(s, e);
    }

    public NotepadException(String s) {
        super(s);
    }

    public NotepadException(Exception e) {
        super(e);
    }

    public ErrorsEnum getError(){
        return error;
    }
}
