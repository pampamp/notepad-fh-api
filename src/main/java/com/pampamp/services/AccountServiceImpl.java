package com.pampamp.services;

import com.pampamp.entities.User;
import com.pampamp.enums.ErrorsEnum;
import com.pampamp.exception.LoginException;
import com.pampamp.exception.NotepadException;
import com.pampamp.repository.UserRepository;
import com.pampamp.security.LoginAttempsChecker;
import com.pampamp.security.TokenEncoder;
import com.pampamp.security.PasswordHash;
import com.pampamp.security.entities.Token;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class AccountServiceImpl implements AccountService{

    private static long TOKEN_TIME_TO_LIVE = 1000L * 60L * 60L * 24L *90L;

    private static final Pattern EMAIL_PATTERN = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{6,20})", Pattern.CASE_INSENSITIVE);

    private static LoginAttempsChecker loginAttempsChecker = new LoginAttempsChecker();


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenEncoder tokenEncoder;

    @Override
    public User authenticateAccount(String email, String password) throws NotepadException {

        if (loginAttempsChecker.isExcedded(email)) {
            throw new LoginException(ErrorsEnum.MAX_LOGIN_ATTENDS_EXEDED.getTitle());
        }

        if (StringUtils.isBlank(password) || StringUtils.isBlank(email)) {
            throw new LoginException(ErrorsEnum.EMAIL_OR_PASSWORD_BLANK.getTitle());
        }

        User user = userRepository.findByEmail(email);

        if (user==null) {
            throw new LoginException(ErrorsEnum.EMAIL_NOT_EXIST.getTitle());
        }

        if (!PasswordHash.validatePassword(password, user.getPassword())) {
            throw new LoginException(ErrorsEnum.INVALID_PASSWORD.getTitle());
        }

        loginAttempsChecker.clear(email);

        Token token = new Token(user.getId(), new Date().getTime() + TOKEN_TIME_TO_LIVE);
        user.setToken(tokenEncoder.encode(token));

        return user;
    }

    private boolean isValidPassword(String password) {
        Matcher matcher = PASSWORD_PATTERN.matcher(password);
        return matcher.matches();
    }

    private boolean isValidEmail(String email) {
        Matcher matcher = EMAIL_PATTERN.matcher(email);
        return matcher.matches();
    }

    public User createAccount(String email, String password, String repassword) throws NotepadException {

        if (!isValidPassword(password)) {
            throw new LoginException(ErrorsEnum.INVALID_PASSWORD_PATTERN.getTitle());
        }

        if (!password.equals(repassword)) {
            throw new LoginException(ErrorsEnum.PASSWORD_NOT_MATCH.getTitle());
        }

        if (!isValidEmail(email)) {
            throw new LoginException(ErrorsEnum.INVALID_EMAIL_PATTERN.getTitle());
        }

        if (userRepository.findByEmail(email)!=null) {
            throw new LoginException(ErrorsEnum.EMAIL_ALREADY_EXIST.getTitle());
        }
        User user = new User();
        user.setEmail(email);
        user.setPassword(PasswordHash.createHash(password));
        userRepository.save(user);

        Token token = new Token(user.getId(), TOKEN_TIME_TO_LIVE);
        user.setToken(tokenEncoder.encode(token));
        return user;
    }



}
