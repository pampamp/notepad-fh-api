package com.pampamp.services;

import com.pampamp.enums.ErrorsEnum;
import com.pampamp.exception.TokenException;
import com.pampamp.security.TokenEncoder;
import com.pampamp.security.entities.Token;
import org.springframework.beans.factory.annotation.Autowired;
import com.pampamp.entities.User;
import com.pampamp.repository.UserRepository;
import org.springframework.stereotype.Service;
import com.pampamp.exception.NotepadException;

@Service
public class UseServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TokenEncoder tokenEncoder;


    @Override
    public User save(User user, String encodedToken) throws NotepadException {

        Token token = tokenEncoder.decode(encodedToken); // TODO Add cache
        if (token.isExpired()) {
            throw new TokenException(ErrorsEnum.TOKEN_EXPIRED.getTitle());
        }
        User tokenUser = this.getUserByToken(encodedToken);
        if (tokenUser==null) {
            new NotepadException(ErrorsEnum.USER_NOT_FOUND.getTitle());
        }
        tokenUser.setFirstname(user.getFirstname());
        tokenUser.setLastname(user.getLastname());
        tokenUser.setAddress(user.getAddress());
        tokenUser.setPhone(user.getPhone());
        tokenUser.setEmail(user.getEmail());
        return userRepository.saveAndFlush(tokenUser);
    }

    @Override
    public User getUserByToken(String encodedToken) throws NotepadException {
        try {
            Token token = tokenEncoder.decode(encodedToken); // TODO Add cache
            if (token.isExpired()) {
                throw new TokenException(ErrorsEnum.TOKEN_EXPIRED.getTitle());
            }
            User user = userRepository.findOne(token.getUserId());
            if (user==null) {
                new NotepadException(ErrorsEnum.USER_NOT_FOUND.getTitle());
            }
            return user;
        } catch (Exception e) {
            throw new NotepadException(ErrorsEnum.GENERAL_ERROR, "SecurityServiceImpl.getUserByToken", e);
        }
    }
}
