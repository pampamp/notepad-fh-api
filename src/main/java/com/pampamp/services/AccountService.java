package com.pampamp.services;

import com.pampamp.entities.User;
import com.pampamp.exception.NotepadException;

public interface AccountService {
    User authenticateAccount(String email, String password) throws NotepadException;
    User createAccount(String email, String password, String repassword) throws NotepadException;
}
