package com.pampamp.services;

import com.pampamp.entities.User;
import com.pampamp.exception.NotepadException;

import java.util.List;

public interface UserService {
    User save(User user, String token) throws NotepadException;
    User getUserByToken(String encodedToken) throws NotepadException;
}
