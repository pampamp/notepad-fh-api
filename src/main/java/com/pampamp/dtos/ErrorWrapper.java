package com.pampamp.dtos;

public class ErrorWrapper {
    private String message;

    public ErrorWrapper(String message) {
        this.message = message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
