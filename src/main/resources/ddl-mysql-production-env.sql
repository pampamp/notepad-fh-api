CREATE DATABASE `libretafh` /*!40100 DEFAULT CHARACTER SET utf8 */;

use libretafh;

 CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

CREATE TABLE `episodes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand` int(11) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `lote` varchar(255) DEFAULT NULL,
  `place` int(11) DEFAULT NULL,
  `treatment` int(11) DEFAULT NULL,
  `units` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_otnvp1972qx7s0tg6uix9wec2` (`user_id`),
  CONSTRAINT `FK_otnvp1972qx7s0tg6uix9wec2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8;